#!/bin/bash

download_src() {
    if [ $# -ne 2 ]; then
        exit 1
    fi

    url=$(grep "${2}_src.master.bundle.url.base" ${1}/${1}.properties | \
          cut -d '=' -f2)
    zip=$(grep "${2}_src.bundle.name" ${1}/${1}.properties | \
          cut -d '=' -f2)

    wget --no-check-certificate ${url}/${zip}
}

download_src "jaxp"  "jaxp"
download_src "jaxws" "jaxws"
download_src "jaxws" "jaf"
