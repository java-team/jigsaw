#
# Copyright (c) 2009, 2010, Oracle and/or its affiliates. All rights reserved.
# DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
#
# This code is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License version 2 only, as
# published by the Free Software Foundation.  Oracle designates this
# particular file as subject to the "Classpath" exception as provided
# by Oracle in the LICENSE file that accompanied this code.
#
# This code is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# version 2 for more details (a copy is included in the LICENSE file that
# accompanied this code).
#
# You should have received a copy of the GNU General Public License version
# 2 along with this work; if not, write to the Free Software Foundation,
# Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
#
# Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
# or visit www.oracle.com if you need additional information or have any
# questions.
#

#
# images            Target to build jigsaw modules images.
#
# This target will build the modules images taking the jdk modules
# under the $outputdir/modules directory. For incremental build,
# make sure that $outputdir/modules is up-to-date (i.e. build
# make/modules/Makefile before invoking the "modules" target).
#
# The following modules images are created:
#
# jre-base-image:   Minimal Java runtime environment
# jre-module-image: Full Java runtime environment with all JRE modules
#                   installed
# jdk-module-image: jre-module-image with all JDK modules installed
#                   i.e. full Java development kit
#
# TODO: we may not need jdk-base-image to build. It is for
#       convenience for the base image testing that at least
#       needs javac in it.
#
# jdk-base-image:   jre-base-image with the language tool modules
#                   installed.
#
#
MODULE_IMAGES_DIR = $(ABS_OUTPUTDIR)
JDK_MODULE_IMAGE_DIR = $(MODULE_IMAGES_DIR)/jdk-module-image
JRE_MODULE_IMAGE_DIR = $(MODULE_IMAGES_DIR)/jre-module-image
JDK_BASE_IMAGE_DIR = $(MODULE_IMAGES_DIR)/jdk-base-image
JRE_BASE_IMAGE_DIR = $(MODULE_IMAGES_DIR)/jre-base-image

#
# TODO:
# 1. Any other sanity check to perform?
# 2. Trim out any extra files not for the jre shipment.
#    (Note the jdk WILL want the jre image before this trimming)
# 3. Post process the image (strips and mcs on Elf files we are shipping)
#    (Note the jdk WILL want the jre image before this processing)
# 4. Sign security jars and export policy files
# 5. Java DB, demos/samples module not installed yet
# 6. man pages - should go in the module of the tool?
#
# Open issues that need further investigation:
# 1. Classes in jre/lib/ext/dnsns.jar are currently put in jre/lib/jndi-dns
#    module.  META-INF/services file is not installed.
# 2. Signed jars
#    For JDK build, signed jars are copied to the build.
#    All jars in the module image are unsigned.
# 3. sparcv9 image is installed on top of a sparc image

#
# Targets.
#
MODULE_TARGET_SUFFIX=
ifeq ($(PLATFORM), solaris)
  ifeq ($(ARCH_DATA_MODEL), 64)
    MODULE_TARGET_SUFFIX=-sol64
  endif
endif

images modules modules-clobber::
	@$(ECHO) ">>>Making "$@" @ `$(DATE)` ..."

images:: modules

#
# jdk-xxx-image depends on jre-xxx-image
#
# Build jdk-xxx-image target before jre-xxx-image target
# so that jre-xxx-image will be made only once
#
modules:: sanity-images post-sanity-images \
	  $(EXPORT_BINARY_PLUGS) \
	  gen-pkgs \
	  jdk-base-image$(MODULE_TARGET_SUFFIX) \
	  jre-base-image$(MODULE_TARGET_SUFFIX) \
	  jdk-module-image$(MODULE_TARGET_SUFFIX) \
	  jre-module-image$(MODULE_TARGET_SUFFIX) \
	  post-image-build

# JDK files
$(JDK_BASE_IMAGE_DIR)/%: $(SHARE_JDK_DOC_SRC)/%
	$(process-doc-file)

# JRE files
$(JRE_BASE_IMAGE_DIR)/%: $(SHARE_JRE_DOC_SRC)/%
	$(process-doc-file)

# Add $(TEXT_SUFFIX) suffix
ifdef TEXT_SUFFIX
$(JRE_BASE_IMAGE_DIR)/%$(TEXT_SUFFIX): $(SHARE_JRE_DOC_SRC)/%
	$(process-doc-file)
endif

# JDK files
$(JDK_MODULE_IMAGE_DIR)/%: $(SHARE_JDK_DOC_SRC)/%
	$(process-doc-file)


# JRE files
$(JRE_MODULE_IMAGE_DIR)/%: $(SHARE_JRE_DOC_SRC)/%
	$(process-doc-file)

ifeq ($(PLATFORM), windows)
$(JRE_BASE_IMAGE_DIR)/README.txt: $(SHARE_JRE_DOC_SRC)/README
	$(process-doc-file)
$(JRE_MODULE_IMAGE_DIR)/README.txt: $(SHARE_JRE_DOC_SRC)/README
	$(process-doc-file)
endif

#
# On Windows dll's are installed into the same direcory as native cmds
ifeq ($(PLATFORM), windows)
    NATLIBS_DIR=bin
else
    # On Mac then dynamic libraries are installed in the lib directory
    ifeq ($(PLATFORM), macosx)
        NATLIBS_DIR=lib
    else
        NATLIBS_DIR=lib/$(LIBARCH)
    endif
endif

#
# Create a module library for the specified image location
define create-module-library
image=$(strip $1); \
mlib=$(MODULE_IMAGES_DIR)/$$image/lib/modules; \
natlib=$(MODULE_IMAGES_DIR)/$$image/$(NATLIBS_DIR); \
natcmd=$(MODULE_IMAGES_DIR)/$$image/bin; \
config=$(MODULE_IMAGES_DIR)/$$image/lib; \
$(ECHO) "Creating module library $$mlib" ; \
$(HOST_JMOD_CMD) create -N $(JMOD_CREATE_ARGS) -L $$mlib \
       --natlib $$natlib --natcmd $$natcmd --config $$config
endef

# Copy module content to the module image
#   $1 - image location
#   $2 - module name
#
define copy-module-content
image=$(strip $1); m=$(strip $2); \
javahome=$(MODULE_IMAGES_DIR)/$$image; \
natlib=$(MODULE_IMAGES_DIR)/$$image/$(NATLIBS_DIR); \
natcmd=$(MODULE_IMAGES_DIR)/$$image/bin; \
config=$(MODULE_IMAGES_DIR)/$$image/lib; \
if [ -d $$m/bin ] ; then \
    $(CP) -rf $(ABS_MODULEPATH_DIR)/$$m/bin/* $$natcmd; \
fi ; \
if [ -d $$m/lib ] ; then \
    $(CP) -rf $(ABS_MODULEPATH_DIR)/$$m/lib/* $$natlib; \
fi ; \
if [ -d $$m/etc ] ; then \
    $(CP) -rf $(ABS_MODULEPATH_DIR)/$$m/etc/* $$config; \
fi
endef

#
# Copy JRE image to the dest
# $1 - jre image location
# $2 - dest location
#
# Shall investigate if we can eliminate the dependency on this special
# copy of the lib/dll files.
# Probably better to copy these files to part of the jdk.base.tools module
# Also, need to copy jawt.lib.
#
# Should *_SUFFIX variable either have "." or not dot?
#
define copy-jre-image
from=$(strip $1); dest=$(strip $2); \
($(CD) $(MODULE_IMAGES_DIR)/$$from && $(FIND) . -depth -print \
              | $(CPIO) -pdum $(MODULE_IMAGES_DIR)/$$dest) ; \
if [ -d "$(ABS_MODULEPATH_DIR)/jdk.base/include" ] ; then \
   $(CP) -rf $(ABS_MODULEPATH_DIR)/jdk.base/include $(MODULE_IMAGES_DIR)/$$dest; \
fi
endef

#
# Install jigsaw module
# Install from the jmod file, if it has been created (see BUILD_PACKAGES)
#   $1 - image location
#   $2 - module name
define install-jigsaw-module
image=$(strip $1); m=$(strip $2); \
mlib=$(MODULE_IMAGES_DIR)/$$image/lib/modules; \
mid=`$(HOST_JMOD_CMD) ls -L $$mlib $$m | $(GREP) -v '#'` ; \
jmodfile=$(ABS_OUTPUTDIR)/jigsaw-pkgs/jmod/$$m@$(MODULE_VERSION).jmod ; \
if [ "x$$mid" = "x" ] ; then \
   $(CD) $(ABS_MODULEPATH_DIR) && \
   if [ -f $$jmodfile ] ; then \
      $(ECHO) "Installing module $$m in $$image, from $$jmodfile" ; \
      $(HOST_JMOD_CMD) install $$jmodfile -L $$mlib || exit 1; \
   else \
      $(ECHO) "Installing module $$m in $$image, from $$m/classes and copy" ; \
      $(HOST_JMOD_CMD) install $$m/classes -L $$mlib $(JMOD_INSTALL_ARGS) $$m || exit 1; \
      $(call copy-module-content,$1,$2) ; \
   fi ; \
else \
   $(ECHO) "Module $$m already installed in $$image" ; \
fi
endef

# Rules for building packages
include $(BUILDDIR)/common/BuildNativePackages.gmk

# set BUILD_PACKAGES=false to skip building jmod packages
#
PKG_TYPES = jmod-pkgs
ifeq ($(BUILD_PACKAGES), false)
  PKG_TYPES =
endif

DEBIAN := $(shell \
               if [ -f /etc/debian_version ] ; then \
                  $(ECHO) true; \
               else \
                  $(ECHO) false; \
               fi)

# FIXME: turn on building debian packages by default once
#        the debian-related issues are fixed (e.g. views, version).
#
BUILD_NATIVE_PACKAGES=false

# set BUILD_NATIVE_PACKAGES=false to skip building native packages
#
ifeq ($(BUILD_NATIVE_PACKAGES), false)
  DEBIAN = false
endif

ifeq ($(DEBIAN), true)
  PKG_TYPES += deb-pkgs
endif

gen-pkgs: $(PKG_TYPES)

%-setup:
	$(RM) -r $(MODULE_IMAGES_DIR)/$*
	$(MKDIR) $(MODULE_IMAGES_DIR)/$*

jdk-base-image: jre-base-image \
                jdk-base-image-setup \
		jdk-base-image-docfiles
	$(call copy-jre-image,jre-base-image,$@)
	@for jm in `$(NAWK) '{print $$1}' $(JDK_BASE_MODULES)` ; do \
	    $(call install-jigsaw-module,$@,$$jm) ; \
	done

jre-base-image: jre-base-image-setup \
		jre-base-image-docfiles
	$(call create-module-library,$@)
	@for jm in `$(NAWK) '{print $$1}' $(JRE_BASE_MODULES)` ; do \
	    $(call install-jigsaw-module,$@,$$jm) ; \
	done

# 64-bit solaris jre image contains only the 64-bit add-on files.
# FIXME: should there be two sets of modules (one for sparc and one for sparcv9)?
jdk-base-image-sol64: jre-base-image-sol64 \
                      jdk-base-image-setup
	@# Temporary workaround - install sparcv9 modules
	$(call copy-jre-image,jre-base-image,jdk-base-image)
	@for jm in `$(NAWK) '{print $$1}' $(JDK_BASE_MODULES)` ; do \
	    $(call install-jigsaw-module,jdk-base-image,$$jm) ; \
	done

jre-base-image-sol64: jre-base-image-setup
	@# Temporary workaround - install sparcv9 modules
	$(call create-module-library,jre-base-image)
	@for jm in `$(NAWK) '{print $$1}' $(JRE_BASE_MODULES)` ; do \
	    $(call install-jigsaw-module,jre-base-image,$$jm) ; \
	done

jdk-base-image-docfiles: $(IMAGE_DOCLIST_JDK:%=$(JDK_BASE_IMAGE_DIR)/%)

jre-base-image-docfiles: $(IMAGE_DOCLIST_JRE:%=$(JRE_BASE_IMAGE_DIR)/%)

jdk-module-image: jre-module-image \
                  jdk-module-image-setup \
		  jdk-module-image-docfiles
	$(call copy-jre-image,jre-module-image,$@)
	@for jm in `$(NAWK) '{print $$1}' $(JDK_MODULES)` ; do \
	    $(call install-jigsaw-module,$@,$$jm); \
	done

jre-module-image: jre-module-image-setup \
  		  jre-module-image-docfiles
	$(call create-module-library,$@)
	@for jm in `$(NAWK) '{print $$1}' $(JRE_MODULES)` ; do \
	    $(call install-jigsaw-module,$@,$$jm); \
	done

# 64-bit solaris jre image contains only the 64-bit add-on files.
# FIXME: should there be two sets of modules (one for sparc and one for sparcv9)?
jdk-module-image-sol64: jre-module-image-sol64 \
                        jdk-module-image-setup \
		        jdk-module-image-docfiles
	@# Temporary workaround - install sparcv9 modules
	$(call copy-jre-image,jre-module-image,jdk-module-image)
	@for jm in `$(NAWK) '{print $$1}' $(JDK_MODULES)` ; do \
	    $(call install-jigsaw-module,jdk-module-image,$$jm); \
	done

jre-module-image-sol64: jre-module-image-setup \
		        jre-module-image-docfiles
	@# Temporary workaround - install sparcv9 modules
	$(call create-module-library,jre-module-image)
	@for jm in `$(NAWK) '{print $$1}' $(JRE_MODULES)` ; do \
	    $(call install-jigsaw-module,jre-module-image,$$jm); \
	done

jdk-module-image-docfiles: $(IMAGE_DOCLIST_JDK:%=$(JDK_MODULE_IMAGE_DIR)/%)

jre-module-image-docfiles: $(IMAGE_DOCLIST_JRE:%=$(JRE_MODULE_IMAGE_DIR)/%)

post-image-build:
	@# Make sure all directories are read/execute for everyone
	@# chmod complains arg list too long; so do it one by one
	$(CHMOD) a+rx `$(FIND) $(JDK_BASE_IMAGE_DIR) -type d`
	$(CHMOD) a+rx `$(FIND) $(JRE_BASE_IMAGE_DIR) -type d`
	$(CHMOD) a+rx `$(FIND) $(JDK_MODULE_IMAGE_DIR) -type d`
	$(CHMOD) a+rx `$(FIND) $(JRE_MODULE_IMAGE_DIR) -type d`

######################################################
# clobber
######################################################
modules-clobber::
	$(RM) -r $(MODULE_IMAGES_DIR)

images modules modules-clobber::
	@$(ECHO) ">>>Finished making "$@" @ `$(DATE)` ..."
	@$(java-vm-cleanup)

.PHONY: modules modules-clobber gen-pkgs \
	jre-base-image$(MODULE_TARGET_SUFFIX) \
	jdk-base-image$(MODULE_TARGET_SUFFIX) \
	jre-module-image$(MODULE_TARGET_SUFFIX) \
	jdk-module-image$(MODULE_TARGET_SUFFIX)

# Force rule
FRC:

