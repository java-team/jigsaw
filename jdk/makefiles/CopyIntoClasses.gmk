#
# Copyright (c) 2011, 2012, Oracle and/or its affiliates. All rights reserved.
# DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
#
# This code is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License version 2 only, as
# published by the Free Software Foundation.  Oracle designates this
# particular file as subject to the "Classpath" exception as provided
# by Oracle in the LICENSE file that accompanied this code.
#
# This code is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
# version 2 for more details (a copy is included in the LICENSE file that
# accompanied this code).
#
# You should have received a copy of the GNU General Public License version
# 2 along with this work; if not, write to the Free Software Foundation,
# Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
#
# Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
# or visit www.oracle.com if you need additional information or have any
# questions.
#

# Copy icu and _dict files used by the text break iterator

COPY_PATTERNS = .icu _dict

# Copy config files for com.sun.org.apache.xml.internal.security

XMLSECURITY_RESOURCEDIR = $(JDK_TOPDIR)/src/share/classes/com/sun/org/apache/xml/internal/security/resource
COPY_FILES += \
  $(XMLSECURITY_RESOURCEDIR)/config.dtd \
  $(XMLSECURITY_RESOURCEDIR)/config.xml

# Copy sun/tools related files into the classes directory.

# Extra jstat files
JSTAT_RESOURCEDIR = $(JDK_TOPDIR)/src/share/classes/sun/tools/jstat/resources
COPY_FILES += \
  $(JSTAT_RESOURCEDIR)/jstat_options \
  $(JSTAT_RESOURCEDIR)/jstat_unsupported_options

# Extra jhat files 
JHAT_RESOURCEDIR = $(JDK_TOPDIR)/src/share/classes/com/sun/tools/hat/resources
COPY_FILES += \
  $(JHAT_RESOURCEDIR)/hat.js \
  $(JHAT_RESOURCEDIR)/oqlhelp.html \
  $(JHAT_RESOURCEDIR)/platform_names.txt

# Extra jrunscript files
JRUNSCRIPT_RESOURCEDIR = $(JDK_TOPDIR)/src/share/classes/com/sun/tools/script/shell
COPY_FILES += \
  $(JRUNSCRIPT_RESOURCEDIR)/init.js \
  $(JRUNSCRIPT_RESOURCEDIR)/messages.properties 

# Extra jvmstat files
COPY_FILES += \
  $(JDK_TOPDIR)/src/share/classes/sun/jvmstat/perfdata/resources/aliasmap

# Servicetag resources
SERVICETAG_RESOURCES_DIR = $(JDK_TOPDIR)/src/share/classes/com/sun/servicetag/resources
COPY_FILES += \
  $(SERVICETAG_RESOURCES_DIR)/product_registration.xsd \
  $(SERVICETAG_RESOURCES_DIR)/register.html \
  $(SERVICETAG_RESOURCES_DIR)/register_ja.html \
  $(SERVICETAG_RESOURCES_DIR)/register_zh_CN.html \
  $(wildcard  $(SERVICETAG_RESOURCES_DIR)/javase_*.properties)

# JConsole resources
JCONSOLE_RESOURCES_DIR = $(JDK_TOPDIR)/src/share/classes/sun/tools/jconsole/resources
COPY_FILES += \
  $(wildcard $(JCONSOLE_RESOURCES_DIR)/*.png) \
  $(wildcard $(JCONSOLE_RESOURCES_DIR)/*.gif)

# Print resources
PRINT_RESOURCES_DIR = $(JDK_TOPDIR)/src/share/classes/sun/print/resources
COPY_FILES += \
  $(wildcard $(PRINT_RESOURCES_DIR)/*.png)

# IDN resources
COPY_FILES += \
  $(JDK_TOPDIR)/src/share/classes/sun/net/idn/uidna.spp

##########################################################################################
#
# Copy the META-INF/services configuration files that are scattered around the source tree
# into classes/META-INF/services. Be aware that META-INF directories that are located at a
# source root (.../classes/META-INF) are automatically copied verbatim by the
# SetupJavaCompilation macro. 
#
# Any other META-INF/services configuration file is found here and platform specific comments
# are uncommented and the configuration file is stored in the output META-INF directory.

# Make sure the output directory is created.
$(shell $(MKDIR) -p $(JDK_OUTPUTDIR)/newclasses/META-INF/services)
# Find all META-INF/services/* files
ALL_META-INF_DIRS_share:=$(shell $(FIND) $(JDK_TOPDIR)/src/share/classes -type d -a -name META-INF)
ALL_META-INF_DIRS_hostapi:=$(shell $(FIND) $(JDK_TOPDIR)/src/$(LEGACY_HOST_OS_API)/classes -type d -a -name META-INF)
# Platform specific overrides shared
ifneq ($(ALL_META-INF_DIRS_hostapi),)
    ALL_META-INF_DIRS:=$(ALL_META-INF_DIRS_hostapi) \
	$(filter-out %$(patsubst $(JDK_TOPDIR)/src/$(LEGACY_HOST_OS_API)/classes%,%,$(ALL_META-INF_DIRS_hostapi)),\
		$(ALL_META-INF_DIRS_share))
else
    ALL_META-INF_DIRS:=$(ALL_META-INF_DIRS_share)
endif
# Filter out META-INF dirs that shouldn't be included
ALL_META-INF_DIRS:=$(filter-out %sun/nio/cs/ext/META-INF,$(ALL_META-INF_DIRS))
ifdef OPENJDK
  ALL_META-INF_DIRS:=$(filter-out %com/sun/script/javascript/META-INF,$(ALL_META-INF_DIRS))
endif

SRC_SERVICES_FILES:=$(wildcard $(addsuffix /services/*,$(ALL_META-INF_DIRS)))
ifdef OPENJDK
    SRC_SERVICES_FILES:=$(filter-out %sun/dc/META-INF/services/sun.java2d.pipe.RenderingEngine,$(SRC_SERVICES_FILES))
    SRC_SERVICES_FILES:=$(filter-out %sun/java2d/cmm/kcms/META-INF/services/sun.java2d.cmm.PCMM,$(SRC_SERVICES_FILES))
else
    SRC_SERVICES_FILES:=$(filter-out %sun/java2d/pisces/META-INF/services/sun.java2d.pipe.RenderingEngine,$(SRC_SERVICES_FILES))
    SRC_SERVICES_FILES:=$(filter-out %sun/java2d/cmm/lcms/META-INF/services/sun.java2d.cmm.PCMM,$(SRC_SERVICES_FILES))
endif
# The number of services files are relatively few. If the increase in numbers, then
# we have to use ListPathsSafelyNow here.
# Change $(JDK_TOPDIR)/src/.../META-INF/services/yyyy into $(JDK_OUTPUTDIR)/newclasses/META-INF/services/yyyy
# The \n in the printf command is needed to make sed work on Solaris.
OUT_SERVICES_FILES:=$(addprefix $(JDK_OUTPUTDIR)/newclasses/META-INF/services/,\
	$(shell $(PRINTF) "$(SRC_SERVICES_FILES)\n" | $(SED) -e 's|/[^ ]*/META-INF/services/||g'))
OUT_SERVICES_FILES_COLON:=$(addsuffix :,$(OUT_SERVICES_FILES))
# Exception handling for print services with no META-INF directory
SRC_SERVICES_FILES_PRINT = $(wildcard $(JDK_TOPDIR)/src/$(LEGACY_HOST_OS_API)/classes/sun/print/services/*)
OUT_SERVICES_FILES_PRINT = $(addprefix $(JDK_OUTPUTDIR)/newclasses/META-INF/services/,\
	$(patsubst $(JDK_TOPDIR)/src/$(LEGACY_HOST_OS_API)/classes/sun/print/services/%,%,\
		$(SRC_SERVICES_FILES_PRINT)))
OUT_SERVICES_FILES_PRINT_COLON = $(addsuffix :,$(OUT_SERVICES_FILES_PRINT))
RULES_SERVICES_PRINT = $(join $(OUT_SERVICES_FILES_PRINT_COLON),$(SRC_SERVICES_FILES_PRINT))

# Now setup the dependency rules to generate a META-INF/services/... from the correct source.
META-INF_RULES_SERVICES:=$(RULES_SERVICES_PRINT) $(join $(OUT_SERVICES_FILES_COLON),$(SRC_SERVICES_FILES))
# Eval the newly created rules to incorporate them into the make tree.
define addto_meta-inf_services
$1
	echo Installing META-INF/services/$$(@F) 
	$(CAT) $$< | $(SED) -e "s/^#\[$(PLATFORM)\]//" > $$@
endef
$(foreach i,$(META-INF_RULES_SERVICES),$(eval $(call addto_meta-inf_services,$i)))
# Here is the generic rule, whose receipt the above rules will trigger.

COPY_EXTRA += $(OUT_SERVICES_FILES)
COPY_EXTRA += $(OUT_SERVICES_FILES_PRINT)
